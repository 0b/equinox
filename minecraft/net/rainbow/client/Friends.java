package net.rainbow.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;

import net.minecraft.client.Minecraft;

public class Friends {

	private static ArrayList<String> friends = new ArrayList<String>();

	public static ArrayList<String> getFriends() {
		return friends;
	}
	
	public static boolean isFriend(String s) {
		return friends.contains(s);
	}

	public static void addFriend(String s) {
		friends.add(s);
		saveFriends();
	}

	public static void delFriend(String s) {
		friends.remove(friends.indexOf(s));
		saveFriends();
	}

	private static void saveFriends() {
		try {
			File file = new File(Minecraft.getMinecraftDir(), "friends.txt");
			BufferedWriter out = new BufferedWriter(new FileWriter(file));
			for (String s : friends) {
				out.write(s);
				out.write("\r\n");
			}
			out.close();
		} catch (Exception e) {
		}
	}

	public static void loadFriends() {
		try {
			File file = new File(Minecraft.getMinecraftDir(), "friends.txt");
			FileInputStream fstream = new FileInputStream(
					file.getAbsolutePath());
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String line;
			while ((line = br.readLine()) != null) {
				String curLine = line.trim();
				addFriend(curLine);
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			saveFriends();
		}
	}

}
