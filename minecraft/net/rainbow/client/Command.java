package net.rainbow.client;

public class Command {
	
	private String name;
	
	public Command(String command) {
		this.name = command;
	}
	
	public String getCmd() {
		return name;
	}
	
	public void runCmd(String[] s) {}
	
	public static void addChat(String s) {
		Client.getPlayer().addChatMessage("\2479[" + Client.getClientTitle() + "] \247f" + s);
	}

}
