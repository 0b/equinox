package net.rainbow.client.hacks;

import net.rainbow.client.Client;

import org.lwjgl.input.Keyboard;

public class Fly extends BaseHack {
	
	public Fly() {
		super("Fly", Keyboard.KEY_R, EnumGuiCategory.MOVEMENT);
	}
	
	@Override
	public void onUpdate() {
		mc.thePlayer.motionX = 0;
		mc.thePlayer.motionY = 0;
		mc.thePlayer.motionZ = 0;
		mc.thePlayer.landMovementFactor = 2;
		mc.thePlayer.jumpMovementFactor = 2;
		if(mc.inGameHasFocus) {
			if(Keyboard.isKeyDown(mc.gameSettings.keyBindJump.keyCode)) {
				mc.thePlayer.motionY += 2 / 2 + 0.2F;
			}
			if(Keyboard.isKeyDown(mc.gameSettings.keyBindSneak.keyCode)) {
				mc.thePlayer.motionY -= 2 / 2 + 0.2F;
			}
		}
	}

}
