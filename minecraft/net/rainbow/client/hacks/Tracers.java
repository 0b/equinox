package net.rainbow.client.hacks;

import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.Entity;
import net.minecraft.src.EntityEnderman;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.EntitySkeleton;
import net.minecraft.src.EntityZombie;
import net.minecraft.src.MathHelper;
import net.minecraft.src.RenderManager;
import net.minecraft.src.Tessellator;
import net.rainbow.client.Friends;

import org.lwjgl.opengl.GL11;

public class Tracers extends BaseHack {
	
	public Tracers() {
		super("Tracers", -1, EnumGuiCategory.RENDER);
	}
	public boolean shouldTrace(EntityLiving var1) {
		if (var1.entityId == mc.thePlayer.entityId) {
			return false;
		} else if (var1 instanceof EntityPlayer) {
			return true;
		} else {
			return false;
		}
	}

	public void setColorByState(EntityLiving var1, float var2) {
		if (var1.hurtTime > 0) {
			GL11.glColor4f(1.0F, 0.0F, 0.0F, var2);
		} else if (var1 instanceof EntityPlayer) {
			EntityPlayer var4 = (EntityPlayer) var1;

			if (Friends.isFriend(var4.username)) {
				GL11.glColor4f(0, 1, 0, var2);
			} else {
				float var10 = mc.thePlayer
						.getDistanceToEntity(var1);
				if (var10 <= 70.0) {
					GL11.glColor4f(1F, var10 / 100F, 0F, var2);
				} else {
					GL11.glColor4f(0.7F, 0.5F, 0.7f, var2);
				}
			}
		} else {
			GL11.glColor4f(1.0F, 1.0F, 1.0F, var2);
		}
	}

	public void setColorByDistance(EntityLiving var1, float var2) {

		if (var1 instanceof EntityPlayer) {
			EntityPlayer var4 = (EntityPlayer) var1;

			if (Friends.isFriend(var4.username)) {
				GL11.glColor4f(0, 1, 0, var2);
				return;
			}
		}

		double var7 = (double) mc.thePlayer
				.getDistanceToEntity(var1);
		float var10 = mc.thePlayer.getDistanceToEntity(var1);
		if (var7 <= 70.0D) {
			GL11.glColor4f(1F, var10 / 100F, 0F, var2);
		} else {
			GL11.glColor4f(0.5f, 0.7f, 0.5f, var2);
		}
	}
	
	

	public void drawESP(double var1, double var3, double var5, EntityLiving var7) {
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glLineWidth(2.0F);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);
		GL11.glDepthMask(false);
		boolean var8 = true;

		if (!(var7 instanceof EntityPlayer)) {
			var8 = false;

			if (mc.thePlayer.getDistanceToEntity(var7) <= 30.0F) {
				var8 = true;
			}
		}

		if (var8) {
			GL11.glPushMatrix();
			GL11.glTranslated(var1, var3, var5);
			GL11.glRotatef(var7.rotationYaw, 0.0F, (float) var3, 0.0F);
			GL11.glTranslated(-var1, -var3, -var5);
			GL11.glTranslated(0.0D, var7.isSneaking() ? 0.2D : 0.1D, 0.0D);
			AxisAlignedBB var9 = AxisAlignedBB.getBoundingBox(var1
					- (double) var7.width, var3, var5 - (double) var7.width,
					var1 + (double) var7.width, var3 + (double) var7.height,
					var5 + (double) var7.width);

			if (var7 instanceof EntityPlayer || var7 instanceof EntityZombie
					|| var7 instanceof EntitySkeleton
					|| var7 instanceof EntityEnderman) {
				var9 = var9.contract(0.25D, 0.0D, 0.25D);
			}

			this.setColorByState(var7, 1.0F);
			drawCrossedOutlinedBoundingBox(var9);
			GL11.glPopMatrix();
		}

		GL11.glDepthMask(true);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_LIGHTING);
	}

	public void renderTracer(Entity var1) {
		GL11.glPushMatrix();
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDepthMask(false);
		EntityLiving var2 = (EntityLiving) var1;
		double var3 = var2.posX;
		double var5 = var2.posY;
		double var7 = var2.posZ;
		double var9 = RenderManager.instance.viewerPosX;
		double var11 = RenderManager.instance.viewerPosY;
		double var13 = RenderManager.instance.viewerPosZ;
		double var15 = var9 - var3;
		double var17 = var11 - var5;
		double var19 = var13 - var7;
		boolean var21 = true;

		if (!(var2 instanceof EntityPlayer)) {
			var21 = false;

			if (mc.thePlayer.getDistanceToEntity(var2) <= 30.0F) {
				var21 = true;
			}
		}

		if (var21) {
			this.setColorByDistance(var2, 0.6F);
			GL11.glLineWidth(3.0F);
			GL11.glBegin(GL11.GL_LINES);
			GL11.glVertex3d(-var15 * 10.0D, -var17 * 10.0D, -var19 * 10.0D);
			GL11.glVertex2d(0.0D, 0.0D);
			GL11.glEnd();
		}

		GL11.glScalef(0.1F, 0.1F, 0.1F);
		GL11.glDepthMask(true);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glPopMatrix();
	}

	public void drawTracers() {
		mc.entityRenderer.disableLightmap(0.0D);

		for (int var1 = 0; var1 < mc.theWorld.loadedEntityList
				.size(); ++var1) {
			Entity var2 = (Entity) mc.theWorld.loadedEntityList
					.get(var1);

			if (var2 instanceof EntityLiving
					&& var2 != mc.thePlayer
					&& this.shouldTrace((EntityLiving) var2)) {
				EntityLiving var3 = (EntityLiving) var2;
				double var4 = var3.posX;
				double var6 = var3.posY;
				double var8 = var3.posZ;
				double var10 = RenderManager.instance.viewerPosX;
				double var12 = RenderManager.instance.viewerPosY;
				double var14 = RenderManager.instance.viewerPosZ;
				double var16 = var10 - var4;
				double var18 = var12 - var6;
				double var20 = var14 - var8;
				this.renderTracer(var3);
				this.drawESP(-var16, -var18, -var20, var3);
			}
		}

		mc.entityRenderer.enableLightmap(0.0D);
	}

	@Override
	public void onRender() {
		this.drawTracers();
	}

	public void onBobbingFinish(float var1) {
		EntityPlayer var2 = (EntityPlayer) mc.renderViewEntity;
		float var3 = var2.distanceWalkedModified
				- var2.prevDistanceWalkedModified;
		float var4 = -(var2.distanceWalkedModified + var3 * var1);
		float var5 = var2.prevCameraYaw + (var2.cameraYaw - var2.prevCameraYaw)
				* var1;
		float var6 = var2.prevCameraPitch
				+ (var2.cameraPitch - var2.prevCameraPitch) * var1;
		GL11.glTranslatef(MathHelper.sin(var4 * (float) Math.PI) * var5 * 0.5F,
				-Math.abs(MathHelper.cos(var4 * (float) Math.PI) * var5), 0.0F);
		GL11.glRotatef(MathHelper.sin(var4 * (float) Math.PI) * var5 * 3.0F,
				0.0F, 0.0F, 1.0F);
		GL11.glRotatef(
				Math.abs(MathHelper.cos(var4 * (float) Math.PI - 0.2F) * var5) * 5.0F,
				1.0F, 0.0F, 0.0F);
		GL11.glRotatef(var6, 1.0F, 0.0F, 0.0F);
	}
	
	public static void drawCrossedOutlinedBoundingBox(AxisAlignedBB var0)
    {
        Tessellator var1 = Tessellator.instance;
        var1.startDrawing(3);
        var1.addVertex(var0.minX, var0.minY, var0.minZ);
        var1.addVertex(var0.maxX, var0.minY, var0.minZ);
        var1.addVertex(var0.maxX, var0.minY, var0.maxZ);
        var1.addVertex(var0.minX, var0.minY, var0.maxZ);
        var1.addVertex(var0.minX, var0.minY, var0.minZ);
        var1.draw();
        var1.startDrawing(3);
        var1.addVertex(var0.minX, var0.maxY, var0.minZ);
        var1.addVertex(var0.maxX, var0.maxY, var0.minZ);
        var1.addVertex(var0.maxX, var0.maxY, var0.maxZ);
        var1.addVertex(var0.minX, var0.maxY, var0.maxZ);
        var1.addVertex(var0.minX, var0.maxY, var0.minZ);
        var1.draw();
        var1.startDrawing(1);
        var1.addVertex(var0.minX, var0.minY, var0.minZ);
        var1.addVertex(var0.minX, var0.maxY, var0.minZ);
        var1.addVertex(var0.maxX, var0.minY, var0.minZ);
        var1.addVertex(var0.maxX, var0.maxY, var0.minZ);
        var1.addVertex(var0.maxX, var0.minY, var0.maxZ);
        var1.addVertex(var0.maxX, var0.maxY, var0.maxZ);
        var1.addVertex(var0.minX, var0.minY, var0.maxZ);
        var1.addVertex(var0.minX, var0.maxY, var0.maxZ);
        var1.addVertex(var0.minX, var0.minY, var0.minZ);
        var1.addVertex(var0.minX, var0.maxY, var0.maxZ);
        var1.addVertex(var0.maxX, var0.minY, var0.minZ);
        var1.addVertex(var0.maxX, var0.maxY, var0.maxZ);
        var1.draw();
    }
	
}