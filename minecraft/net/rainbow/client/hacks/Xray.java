package net.rainbow.client.hacks;

import java.util.Arrays;
import java.util.List;

import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.RenderManager;
import net.minecraft.src.Tessellator;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

public class Xray extends BaseHack {
	
	public Xray() {
		super("Xray", Keyboard.KEY_X, EnumGuiCategory.RENDER);
	}
	
	@Override
	public void onToggle() {
		mc.renderGlobal.loadRenderers();
	}

}
