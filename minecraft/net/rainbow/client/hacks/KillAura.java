package net.rainbow.client.hacks;

import net.minecraft.src.Entity;
import net.minecraft.src.EntityAnimal;
import net.minecraft.src.EntityLiving;
import net.minecraft.src.EntityMob;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.EnumMovingObjectType;
import net.minecraft.src.Material;
import net.minecraft.src.MathHelper;
import net.rainbow.client.Client;
import net.rainbow.client.Command;
import net.rainbow.client.Friends;

import org.lwjgl.input.Keyboard;

public class KillAura extends BaseHack {
	
	public float range = 3.8F, savedYaw, savedPitch;
	public int speed;
	public boolean safety;
	int tick;
	
	public KillAura() {
		super("KillAura", Keyboard.KEY_F, EnumGuiCategory.COMBAT);
		
		Client.addCmd(new Command("ar") {
			@Override
			public void runCmd(String[] s) {
				try {
					float auraRange = Float.parseFloat(s[1]);
					range = auraRange;
					addChat("New aura range: " + range);
				} catch (Exception e) { addChat("Wrong syntax: .ar <range>"); }
			}
		});
		
		Client.addCmd(new Command("as") {
			@Override
			public void runCmd(String[] s) {
				try {
					int auraSpeed = Integer.parseInt(s[1]);
					speed = auraSpeed;
					addChat("New aura speed: " + speed + " ticks.");
				} catch (Exception e) { addChat("Wrong syntax: .as <integer>"); }
			}
		});
		
		Client.addCmd(new Command("safety") {
			@Override
			public void runCmd(String[] s) {
				safety = !safety;
				addChat("Safety toggled " + (safety ? "on." : "off."));
			}
		});
	}
	
	@Override
	public void onUpdate() {
		++tick;
		EntityLiving target = getNearestEntity();
		if(target != null) {
			if(mc.thePlayer.canEntityBeSeen(target) && !target.isDead) {
				if(!safety) {
					savedYaw = mc.thePlayer.rotationYaw;
					savedPitch = mc.thePlayer.rotationPitch;
					faceEntity(target);
				}
				if(tick > 6) {
					mc.thePlayer.swingItem();
					mc.playerController.attackEntity(mc.thePlayer, target);
					tick = 0;
				}
				if(!safety) {
					mc.thePlayer.rotationYaw = savedYaw;
					mc.thePlayer.rotationPitch = savedPitch;
				}
			}
		}
	}
	
	public EntityLiving getNearestEntity() {
		EntityLiving target = null;
		for(Object o : Client.getWorld().loadedEntityList) {
			if(o instanceof EntityLiving) {
				EntityLiving e = (EntityLiving)o;
				if(mc.thePlayer.getDistanceToEntity(e) <= range && isAttackable(e)) {
					if(safety) {
						
						if(mc.objectMouseOver == null || mc.objectMouseOver.typeOfHit != EnumMovingObjectType.ENTITY) {
							continue;
						}
					}
					if(e instanceof EntityPlayer) {
						if(Friends.isFriend(((EntityPlayer)e).username)) {
							continue;
						}
					}
					if(target == null) {
						target = e;
					} else if(mc.thePlayer.getDistanceToEntity(e) < mc.thePlayer.getDistanceToEntity(target)) {
						target = e;
					}
				}
			}
		}
		return target;
	}
	
	public boolean isAttackable(Entity e) {
		return (e instanceof EntityMob || e instanceof EntityAnimal || e instanceof EntityPlayer) && e.isEntityAlive() && e != mc.thePlayer;
	}
	
	public void faceEntity(Entity entity) {
		double x = entity.posX - mc.thePlayer.posX;
		double z = entity.posZ - mc.thePlayer.posZ;
		double y = entity.posY + (entity.getEyeHeight()/1.4D) - mc.thePlayer.posY + (mc.thePlayer.getEyeHeight()/1.4D);
		double helper = MathHelper.sqrt_double(x * x + z * z);

		float newYaw = (float)((Math.toDegrees(-Math.atan(x / z))));
		float newPitch = (float)-Math.toDegrees(Math.atan(y / helper));

		if(z < 0 && x < 0) { newYaw = (float)(90D + Math.toDegrees(Math.atan(z / x))); }
		else if(z < 0 && x > 0) { newYaw = (float)(-90D + Math.toDegrees(Math.atan(z / x))); }

		mc.thePlayer.rotationYaw = newYaw; 
		mc.thePlayer.rotationPitch = newPitch;
    }
}
