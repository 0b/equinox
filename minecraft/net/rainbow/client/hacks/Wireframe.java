package net.rainbow.client.hacks;

import org.lwjgl.opengl.GL11;

public class Wireframe extends BaseHack {

	public Wireframe() {
		super("Wireframe", -1, EnumGuiCategory.RENDER);
	}
	
	@Override
	public void onToggle() {
		mc.renderGlobal.loadRenderers();
	}
	
	public static void preWireframe() {
		GL11.glPushAttrib(GL11.GL_ALL_ATTRIB_BITS);
        GL11.glEnable(GL11.GL_POLYGON_OFFSET_FILL);
        GL11.glPolygonOffset(-1f, -1f);
        GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
	}
	
	public static void postWireframe() {
		GL11.glPopAttrib();
	}
	
}
