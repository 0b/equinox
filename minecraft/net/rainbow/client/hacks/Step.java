package net.rainbow.client.hacks;

import net.rainbow.client.Client;

import org.lwjgl.input.Keyboard;

public class Step extends BaseHack {
	
	public Step() {
		super("Step", Keyboard.KEY_COMMA, EnumGuiCategory.MOVEMENT);
	}
	
	@Override
	public void onUpdate() {
		if(Client.getPlayer().isCollidedHorizontally && Client.getPlayer().onGround) {
			Client.getPlayer().jump();
		}
	}
}
