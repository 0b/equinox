package net.rainbow.client.hacks;

import net.rainbow.client.Client;
import net.rainbow.client.ui.matte.MatteScreen;

import org.lwjgl.input.Keyboard;

public class ClickGui extends BaseHack {
	
	public ClickGui()  {
		super("ClickGui", Keyboard.KEY_GRAVE, EnumGuiCategory.NONE);
	}
	
	@Override
	public void onToggle() {
		Client.getMC().displayGuiScreen(new MatteScreen());
	}

}
