package net.rainbow.client.hacks;

import net.rainbow.client.Client;

import org.lwjgl.input.Keyboard;

public class Freecam extends BaseHack {

	private double freecamX, freecamY, freecamZ;
	private float freecamPitch, freecamYaw;

	public Freecam() {
		super("Freecam", Keyboard.KEY_BACK, EnumGuiCategory.MOVEMENT);
	}

	@Override
	public void onEnable() {
		freecamX = mc.thePlayer.posX;
		freecamY = mc.thePlayer.posY;
		freecamZ = mc.thePlayer.posZ;
		freecamPitch = mc.thePlayer.rotationPitch;
		freecamYaw = mc.thePlayer.rotationYaw;
		
		mc.thePlayer.noClip = true;
		if(!Client.getHack("Fly").getState())
			Client.getHack("Fly").toggle();
	}

	@Override
	public void onDisable() {
		mc.thePlayer.noClip = false;
		if(Client.getHack("Fly").getState())
			Client.getHack("Fly").toggle();
		mc.thePlayer.setPositionAndRotation(freecamX, freecamY, freecamZ, freecamYaw, freecamPitch);
	}
}
