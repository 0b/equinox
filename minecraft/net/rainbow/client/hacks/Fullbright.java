package net.rainbow.client.hacks;

import org.lwjgl.input.Keyboard;

public class Fullbright extends BaseHack {
	
	public Fullbright() {
		super("Fullbright", Keyboard.KEY_C, EnumGuiCategory.WORLD);
	}

}
