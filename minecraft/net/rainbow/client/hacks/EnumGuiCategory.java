package net.rainbow.client.hacks;

import net.rainbow.client.Client;

public enum EnumGuiCategory {
    
    MOVEMENT(0xFF13C422), COMBAT(0xFFC70000), WORLD(0xFF6495ED), RENDER(
            0xFF474790), BLOCK(0xFF2899CD), CHAT(), MISC(), COMMAND(), NONE();
   
    private int color;
    private boolean gui;
   
    private EnumGuiCategory() {
            color = -1;
            gui = false;
    }
   
    private EnumGuiCategory(final int color) {
            this.color = color;
            gui = true;
            Client.getCategories().add(this);
    }
   
    public boolean getInGui() {
            return gui;
    }
   
    public int getModuleColor() {
            return color;
    }
   
}
