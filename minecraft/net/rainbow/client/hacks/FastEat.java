package net.rainbow.client.hacks;

import net.minecraft.src.Packet10Flying;
import net.minecraft.src.Potion;

public class FastEat extends BaseHack {
	
	private int packets = 15000;
	
	public FastEat() {
		super("FastEat", -1, EnumGuiCategory.MISC);
	}
	
	@Override
    public void onUpdate(){
        if(this.canSendPackets()){
            for(int x = 0; x < packets; x++){
                mc.getNetHandler().addToSendQueue(new Packet10Flying(mc.thePlayer.onGround));
            }
        }
    }

    private boolean canSendPackets() {
    	return  !mc.thePlayer.handleLavaMovement()
                && !mc.thePlayer.handleWaterMovement()
                && mc.thePlayer.onGround
                && mc.thePlayer.hurtTime <= 0
                && !mc.thePlayer.isBurning()
                && !mc.thePlayer.isPotionActive(Potion.poison)
                && mc.thePlayer.isEating();
    }
}
