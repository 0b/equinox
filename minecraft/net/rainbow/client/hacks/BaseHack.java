package net.rainbow.client.hacks;

import net.minecraft.client.Minecraft;
import net.minecraft.src.Entity;

public class BaseHack {
	
	private String name;
	private int bind;
	private boolean state;
	private EnumGuiCategory category;
	public Minecraft mc = Minecraft.getMinecraft();
	
	public BaseHack(String name, int bind, EnumGuiCategory c) {
		this.name = name;
		this.bind = bind;
		this.category = c;
	}
	
	public boolean getState() {
		return state;
	}
	
	public int getBind() {
		return this.bind;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getColor() {
		return this.category.getModuleColor();
	}
	
	public EnumGuiCategory getCategory() {
		return this.category;
	}
	
	public void toggle() {
		onToggle();
		if(this.state) {
			onDisable();
		} else {
			onEnable();
		}
		this.state = !this.state;
	}
	
	public void onUpdate() {}
	public void onEnable() {}
	public void onDisable() {}
	public void onToggle() {}
	public void onAttack(Entity e) {}
	public void onRender() {}
	public void onCommand(String s) {}
	
}
