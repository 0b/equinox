package net.rainbow.client.hacks;

import net.minecraft.src.ItemStack;
import net.minecraft.src.Packet15Place;
import net.minecraft.src.Packet16BlockItemSwitch;

public class Soup extends BaseHack {
	
	public Soup() {
		super("Soup", -1, EnumGuiCategory.COMBAT);
	}
	private boolean inInventory;
	private int invSlot;
	private ItemStack is;
	private int stackSlot = -1;
	private long currentMS;
	private long lastStack = -1;
	private long stackThreshhold = 150L;
	static int soups;

	private int oldSlot;

	@Override
	public void onUpdate() {
		if (mc.thePlayer.getHealth() <= 14) {
			eatSoup();
		}
	}

	private void eatSoup() {
		currentMS = System.nanoTime() / 1000000;
		oldSlot = mc.thePlayer.inventory.currentItem;
		if (timeHasPassed(stackThreshhold)) {
			stackBowl();
			lastStack = System.nanoTime() / 1000000;
		}
		invSlot = -1;
		is = null;
		for (int slot = 44; slot >= 9; slot--) {
			is = getStackAt(slot);
			if (is == null) {
				continue;
			}
			if (is.itemID == 282) {
				if (slot >= 36 && slot <= 44) {
					int theSlot = invSlot - 36;
					if (mc.thePlayer.inventory.currentItem != theSlot) {
						mc.thePlayer.inventory.currentItem = slot - 36;
						mc.getNetHandler().addToSendQueue(
								new Packet16BlockItemSwitch(
										mc.thePlayer.inventory.currentItem));
					}
					mc.playerController.updateController();
					eatHeldItem();
					break;
				}
				invSlot = slot;
				break;
			}
		}
		if (is == null) {
			return;
		}
		if (invSlot != -1) {
			mc.playerController.windowClick(0, invSlot, 0, 1, mc.thePlayer);
			mc.playerController.updateController();
		}

		mc.thePlayer.inventory.currentItem = oldSlot;

	}

	private boolean timeHasPassed(long threshhold) {
		return currentMS - lastStack >= threshhold;
	}

	private void eatHeldItem() {
		mc.getNetHandler().addToSendQueue(
				new Packet15Place(-1, -1, -1, 255, mc.thePlayer
						.getCurrentEquippedItem(), 0F, 0F, 0F));
		mc.playerController.updateController();
	}

	private ItemStack getStackAt(int slot) {
		return mc.thePlayer.inventoryContainer.getSlot(slot).getStack();
	}

	private void stackBowl() {
		for (int slot = 44; slot >= 9; slot--) {
			is = getStackAt(slot);

			if (is == null) {
				if (!(stackSlot >= 36 && stackSlot <= 44)) {
					stackSlot = slot;
				}
				continue;
			}

			if (is.itemID == 281) {
				if (stackSlot != -1) {
					if (is.stackSize >= 64) {
						continue;
					}
					mc.playerController
							.windowClick(0, slot, 0, 0, mc.thePlayer);
					mc.playerController.windowClick(0, stackSlot, 0, 0,
							mc.thePlayer);
					mc.playerController.updateController();
					break;
				} else {
					stackSlot = slot;
				}
			}
		}
		stackSlot = -1;
	}

	private void updateSoupCount() {
		soups = 0;

		for (int i = 44; i >= 9; i--) {
			ItemStack stack = mc.thePlayer.inventory.getStackInSlot(i);

			if ((stack != null) && (stack.itemID == 282))
				soups += 1;
		}
	}
	
	public static int getSoups() {
		return soups;
	}
}
