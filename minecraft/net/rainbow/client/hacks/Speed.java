package net.rainbow.client.hacks;

import net.rainbow.client.Client;

import org.lwjgl.input.Keyboard;

public class Speed extends BaseHack {
	
	public Speed() {
		super("Speed", Keyboard.KEY_V, EnumGuiCategory.MOVEMENT);
	}
	
	@Override
	public void onUpdate() {
		if(Keyboard.isKeyDown(Client.getMC().gameSettings.keyBindForward.keyCode))
			Client.getPlayer().setSprinting(true);
	}

}
