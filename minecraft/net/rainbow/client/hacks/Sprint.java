package net.rainbow.client.hacks;

import net.minecraft.src.PotionEffect;
import net.rainbow.client.Client;

import org.lwjgl.input.Keyboard;

public class Sprint extends BaseHack {
	
	public Sprint() {
		super("Sprint", Keyboard.KEY_K, EnumGuiCategory.MOVEMENT);
	}
	
	@Override
	public void onUpdate() {
		if(Keyboard.isKeyDown(Client.getMC().gameSettings.keyBindForward.keyCode))
			Client.getPlayer().setSprinting(true);
	}

}
