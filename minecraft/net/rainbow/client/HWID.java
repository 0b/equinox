package net.rainbow.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.swing.JOptionPane;

public class HWID {

	public static void HWIDc() {
		try {
			boolean hasPaid = false;
			String hwid = getMD5((System.getenv("PROCESSOR_IDENTIFIER")
					+ System.getenv("COMPUTERNAME") + System
					.getProperty("user.name")).trim());
			URL url = new URL("http://aa3.info/hwid.txt");
			URLConnection urlC = url.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					urlC.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				if (inputLine.equalsIgnoreCase(hwid)) {
					hasPaid = true;
				}
			}
			in.close();
			if (!hasPaid) {
				JOptionPane.showInputDialog(null, "HWID", hwid);
				System.exit(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	private static String getMD5(String input) {
		String res = "";
		try {
			MessageDigest algorithm = MessageDigest.getInstance("MD5");
			algorithm.reset();
			algorithm.update(input.getBytes());
			byte[] md5 = algorithm.digest();
			String tmp = "";
			for (int i = 0; i < md5.length; i++) {
				tmp = (Integer.toHexString(0xFF & md5[i]));
				if (tmp.length() == 1) {
					res += "0" + tmp;
				} else {
					res += tmp;
				}
			}
		} catch (NoSuchAlgorithmException ex) {
		}
		return res;
	}

}
