package net.rainbow.client;

import java.util.ArrayList;

import net.minecraft.client.Minecraft;
import net.minecraft.src.EntityClientPlayerMP;
import net.minecraft.src.FontRenderer;
import net.minecraft.src.PlayerControllerMP;
import net.minecraft.src.WorldClient;
import net.rainbow.client.hacks.BaseHack;
import net.rainbow.client.hacks.Chests;
import net.rainbow.client.hacks.ClickGui;
import net.rainbow.client.hacks.Drugs;
import net.rainbow.client.hacks.EnumGuiCategory;
import net.rainbow.client.hacks.FastEat;
import net.rainbow.client.hacks.Fly;
import net.rainbow.client.hacks.Freecam;
import net.rainbow.client.hacks.Fullbright;
import net.rainbow.client.hacks.KillAura;
import net.rainbow.client.hacks.Soup;
import net.rainbow.client.hacks.Speed;
import net.rainbow.client.hacks.Sprint;
import net.rainbow.client.hacks.Step;
import net.rainbow.client.hacks.Tracers;
import net.rainbow.client.hacks.Wireframe;
import net.rainbow.client.hacks.Xray;

public class Client {
	
	static ArrayList<BaseHack> hacks = new ArrayList<BaseHack>();
	static ArrayList<BaseHack> enabled = new ArrayList<BaseHack>();
	static ArrayList<Command> cmds = new ArrayList<Command>();
	static ArrayList<EnumGuiCategory> categories = new ArrayList<EnumGuiCategory>();
	
	public static void init() {
		//HWID.HWIDc();
		hacks.add(new Sprint());
		hacks.add(new KillAura());
		hacks.add(new Fullbright());
		hacks.add(new Step());
		hacks.add(new Fly());
		hacks.add(new Speed());
		hacks.add(new ClickGui());
		hacks.add(new Soup());
		hacks.add(new Tracers());
		hacks.add(new Wireframe());
		hacks.add(new Xray());
		hacks.add(new Freecam());
		hacks.add(new Chests());
		hacks.add(new FastEat());
		hacks.add(new Drugs());
		System.out.println(hacks.size() + " hacks loaded.");
		addCmd(new Command("help") {
			@Override
			public void runCmd(String[] s) {
				String s1 = "Commands: ";
				for(Command c : getCmds()) {
					s1 += c.getCmd() + ", ";
				}
				for(BaseHack h : getHacks()) {
					s1 += h.getName().toLowerCase().replaceAll(" ", "") + (getHacks().indexOf(h) + 1 == getHacks().size() ? "" : ", ");
				}
				addChat(s1);
			}
		});
		
	}
	
	public static String getClientTitle() {
		return "Equinox";
	}

    public static String getClientVersion() {
        return "1.2";
    }
	
	public static Minecraft getMC() {
		return Minecraft.getMinecraft();
	}
	
	public static EntityClientPlayerMP getPlayer() {
		return getMC().thePlayer;
	}
	
	public static PlayerControllerMP getController() {
		return getMC().playerController;
	}
	
	public static WorldClient getWorld() {
		return getMC().theWorld;
	}
	
	public static FontRenderer getFR() {
		return getMC().fontRenderer;
	}
	
	public static ArrayList<BaseHack> getHacks() {
		return hacks;
	}
	
	public static ArrayList<Command> getCmds() {
		return cmds;
	}
	
	public static void addCmd(Command c) {
		cmds.add(c);
	}
	
	public static BaseHack getHack(String s) {
		for(BaseHack h : getHacks()) {
			if(h.getName().equalsIgnoreCase(s))
				return h;
		}
		return null;
	}
	
	public static ArrayList<EnumGuiCategory> getCategories() {
		return categories;
	}
	
	public static ArrayList<BaseHack> getHacksInCategory(EnumGuiCategory c) {
		ArrayList<BaseHack> list = new ArrayList<BaseHack>();
		for(BaseHack h : getHacks()) {
			if(h.getCategory() == c) {
				list.add(h);
			}
		}
		
		return list;
	}

}
