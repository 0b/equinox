package net.rainbow.client.ui.click;

import java.util.ArrayList;

import net.minecraft.src.GuiScreen;
import net.rainbow.client.Client;
import net.rainbow.client.hacks.BaseHack;
import net.rainbow.client.hacks.EnumGuiCategory;
import net.rainbow.client.ui.click.element.Button;
import net.rainbow.client.ui.click.element.Panel;

public class GuiClick extends GuiScreen {
	
	public static ArrayList<Panel> panels = new ArrayList<Panel>();
	public static int panelX = 3;
	
	public GuiClick() {
		if(panels.isEmpty()) {
			this.loadPanels();
		}
	}
	
	public void loadPanels() {
		panels.add(new Panel("Combat") {
			public void addElements() {
				for(BaseHack h : Client.getHacksInCategory(EnumGuiCategory.COMBAT)) {
					this.elements.add(new Button(h));
				}
				this.x = panelX;
				panelX += 102;
			}
		});
		panels.add(new Panel("Player") {
			public void addElements() {
				for(BaseHack h : Client.getHacksInCategory(EnumGuiCategory.MOVEMENT)) {
					this.elements.add(new Button(h));
				}
				this.x = panelX;
				panelX += 102;
			}
		});
		panels.add(new Panel("World") {
			public void addElements() {
				for(BaseHack h : Client.getHacksInCategory(EnumGuiCategory.WORLD)) {
					this.elements.add(new Button(h));
				}
				this.x = panelX;
				panelX += 102;
			}
		});
		panels.add(new Panel("Render") {
			public void addElements() {
				for(BaseHack h : Client.getHacksInCategory(EnumGuiCategory.RENDER)) {
					this.elements.add(new Button(h));
				}
				this.x = panelX;
				panelX += 102;
			}
		});
		panels.add(new Panel("Misc") {
			public void addElements() {
				for(BaseHack h : Client.getHacksInCategory(EnumGuiCategory.MISC)) {
					this.elements.add(new Button(h));
				}
				this.x = panelX;
				panelX += 102;
			}
		});
	}
	
	public boolean doesGuiPauseGame() {
		return false;
	}
	
	protected void mouseClicked(int par1, int par2, int par3) {
		for(int x = 0; x < panels.size(); x++) {
			Panel p = panels.get(x);
			if(p.isPanelOnTop(par1, par2)) {
				if(x == panels.size() - 1) {
					p.mouseClicked(par1, par2, par3);
				}
				Panel temp = p;
				panels.remove(p);
				panels.add(temp);
			}
		}
		super.mouseClicked(par1, par2, par3);
	}
	
	protected void mouseMovedOrUp(int par1, int par2, int par3) {
		if(par3 == 0) {
			for(Panel p : panels) {
				p.dragging = false;
			}
		}
		super.mouseMovedOrUp(par1, par2, par3);
	}
	
	public void drawScreen(int par1, int par2, float par3) {
		this.drawDefaultBackground();
		for(Panel p : panels) {
			p.drawPanel(par1, par2);
		}
		super.drawScreen(par1, par2, par3);
	}

}
