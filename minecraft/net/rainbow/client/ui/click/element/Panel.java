package net.rainbow.client.ui.click.element;

import java.util.ArrayList;

import net.minecraft.client.Minecraft;
import net.minecraft.src.Gui;
import net.rainbow.client.Client;
import net.rainbow.client.ui.click.GuiClick;
import net.rainbow.client.ui.font.CFontRenderer;

public class Panel {
	
	public int x = 3, y = 3, height = 100, width = 100, dragX, dragY;
	public boolean dragging;
	public String name;
	public ArrayList<PanelElement> elements = new ArrayList<PanelElement>();
	
	private CFontRenderer fontRenderer  = new CFontRenderer(Minecraft.getMinecraft(), "Segoe UI Light", 18);
	
	public Panel(String name) {
		this.name = name;
		addElements();
	}
	
	public void addElements() {}
	
	public void drawPanel(int par1, int par2) {
		if(this.dragging) {
			this.x = dragX + par1;
			this.y = dragY + par2;
		}
		Gui.drawRect(this.x - 1, this.y - 1, this.x + this.width + 1, this.y + height + 1, 0x66555555);
		Gui.drawRect(this.x, this.y, this.x + this.width, this.y + 10, 0x66666666);
		Gui.drawRect(this.x, this.y + 12, this.x + this.width, this.y + this.height, 0x66666666);
		fontRenderer.drawStringWithShadow(name, this.x + 3, this.y + 1, 0xFFFFFF);
		int index = 0;
		for(int x = 0; x < elements.size(); x++) {
			PanelElement e  = elements.get(x);
			e.draw(this.x, this.y + index + 13, par1, par2);
			index += e.height;
		}
		this.height = index + 15;
	}
	
	public void mouseClicked(int par1, int par2, int par3) {
		if(par3 == 0) {
			if(isOverPanelTop(par1, par2)) {
				this.dragging = true;
				this.dragX = this.x - par1;
				this.dragY = this.y - par2;
				Client.getMC().sndManager.playSoundFX("random.click", 1F, 1F);
			}
			
			for(PanelElement e : elements) {
				e.handleClicks(par1, par2, par3);
			}
		}
	}
	
	public boolean isOverPanel(int par1, int par2) {
		return par1 >= this.x && par1 <= this.x + this.width && par2 >= this.y && par2 <= this.y + this.height;
	}
	
	public boolean isOverPanelTop(int par1, int par2) {
		return par1 >= this.x && par1 <= this.x + this.width && par2 >= this.y && par2 <= this.y + 10;
	}
	
	public boolean isPanelOnTop(int par1, int par2) {
		if(isOverPanel(par1, par2)) {
			for(Panel p : GuiClick.panels) {
				if(p.isOverPanel(par1, par2) && GuiClick.panels.indexOf(p) > GuiClick.panels.indexOf(this)) {
					return false;
				}
				return true;
			}
		}
		return false;
	}

}
