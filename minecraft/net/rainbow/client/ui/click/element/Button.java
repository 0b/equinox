package net.rainbow.client.ui.click.element;

import net.minecraft.client.Minecraft;
import net.minecraft.src.Gui;
import net.rainbow.client.Client;
import net.rainbow.client.hacks.BaseHack;
import net.rainbow.client.ui.font.CFontRenderer;

public class Button extends PanelElement {
	
	public BaseHack hack;
	public String hackName;
	
	private CFontRenderer fontRenderer  = new CFontRenderer(Minecraft.getMinecraft(), "Segoe UI Light", 18);
	
	public Button(BaseHack hack) {
		this.hack = hack;
		this.hackName = hack.getName();
		this.height = 15;
	}
	
	public void handleClicks(int par1, int par2, int par3) {
		if(this.isOverButton(par1, par2)) {
			hack.toggle();
			Client.getMC().sndManager.playSoundFX("random.click", 1F, 1F);

		}
	}
	
	public void draw(int par1, int par2, int par3, int par4) {
		super.draw(par1, par2, par3, par4);
		Gui.drawRect(this.x + 2, this.y + 1, this.x + 98, this.y + 14, hack.getState() ? 0xFF01588F : 0xFF666666);
		fontRenderer.drawStringWithShadow(hack.getName(), this.x + 4, this.y + 4, 0xFFFFFF);
	}
	
	public boolean isOverButton(int par1, int par2) {
		return par1 >= this.x + 2 && par1 <= this.x + 98 && par2 >= this.y + 1 && par2 <= this.y + 14;
	}

}
