package net.rainbow.client.ui;

import net.minecraft.src.Gui;
import net.rainbow.client.Client;
import net.rainbow.client.hacks.BaseHack;

public class InGameGUI {

	static int yPos, rectYPos;
	
	public static void renderIGGUI() {
		yPos = 2;
		rectYPos = 2;

		for(BaseHack h : Client.getHacks()) {
			if(h.getState() && h.getCategory().getInGui()) {
				rectYPos += 11;
			}
		}
		
		Gui.drawRect(0, 0, getLongest() + 4, rectYPos, 0x66000000);
		for(BaseHack h : Client.getHacks()) {
			if(h.getState() && h.getCategory().getInGui()) {
				Client.getFR().drawStringWithShadow(h.getName(), 2, yPos, h.getColor());
				yPos += 11;
			}
		}
	}
	
	static int getLongest() {
		String longestThing = null;
		for(BaseHack h : Client.getHacks()) {
			if(h.getState() && h.getCategory().getInGui()) {
				if(Client.getFR().getStringWidth(h.getName()) > Client.getFR().getStringWidth(longestThing)) {
					longestThing = h.getName();
				}
			}
		}
		return (longestThing == null) ? 0 : Client.getFR().getStringWidth(longestThing);
	}
	
}
