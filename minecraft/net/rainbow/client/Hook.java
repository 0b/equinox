package net.rainbow.client;

import net.minecraft.src.Entity;
import net.rainbow.client.hacks.BaseHack;

public class Hook {
	
	public static void onKey(int key) {
		for(BaseHack h : Client.getHacks()) {
    		if(h.getBind() == key)
    			h.toggle();
    	}
	}
	
	public static void onUpdate() {
		for(BaseHack h : Client.getHacks()) {
			if(h.getState())
				h.onUpdate();
		}
	}
	
	public static void onAttack(Entity e) {
		for(BaseHack h : Client.getHacks()) {
			if(h.getState())
				h.onAttack(e);
		}
	}
	
	public static void onRender() {
		for(BaseHack h : Client.getHacks()) {
			if(h.getState())
				h.onRender();
		}
	}
	
	public static boolean doCommands(String s) {
		if(!s.startsWith(".")) return false;
		String[] s1 = s.split(".");
		for(Command c : Client.getCmds()) {
			if(s.replaceFirst(".", "").startsWith(c.getCmd())) {
				try {
					c.runCmd(s.split(" "));
				} catch(Exception e) { c.runCmd(new String[] {s}); }
				return true;
			}
		}
		
		for(BaseHack c : Client.getHacks()) {
			if(s.replaceFirst(".", "").startsWith(c.getName().toLowerCase())) {
				c.toggle();
				Command.addChat(c.getName() + (c.getState() ? " enabled." : " disabled."));
				return true;
			}
		}
		
		Command.addChat("Invalid command. Use .help for help.");
		return true;
	}

}
